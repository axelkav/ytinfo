FROM python:3.8-alpine
ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache mariadb-connector-c-dev ;\
    apk add --no-cache --virtual .build-deps \
        build-base \
        musl-dev \
        libc-dev \
        mariadb-dev \
        python3-dev \
        gcc

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
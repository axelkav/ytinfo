.PHONY: superuser install build up down ps clean syncdb

superuser:
	-@docker-compose -f docker-compose.yml up -d
	-@docker-compose -f docker-compose.yml exec api python src/manage.py createsuperuser

install:
	-@docker-compose -f docker-compose.yml up -d --build
	-@docker-compose -f docker-compose.yml exec api python src/manage.py migrate

build:
	-@docker-compose -f docker-compose.yml build

up:
	-@docker-compose -f docker-compose.yml up -d

down:
	-@docker-compose -f docker-compose.yml down

ps:
	-@docker-compose -f docker-compose.yml ps

syncdb:
	-@docker-compose -f docker-compose.yml exec api python src/manage.py makemigrations
	-@docker-compose -f docker-compose.yml exec api python src/manage.py migrate

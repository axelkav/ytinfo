import os
import celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ytinfo.settings')

app = celery.Celery('ytinfo')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
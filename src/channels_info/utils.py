import os

import googleapiclient.discovery

from django.conf import settings


def get_yt_id_from_yt_username(username):
    """ get youtube ID from the link """
    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = settings.GOOGLE_YT_API_KEY

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey = DEVELOPER_KEY
    )

    request = youtube.channels().list(
        part="id",
        forUsername=username
    )
    response = request.execute()
    return response['items'][0]['id']


def send_search_video_request(channel_id, next_page_token=""):
    """ search videos related to channel and get video details """
    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = settings.GOOGLE_YT_API_KEY

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey = DEVELOPER_KEY
    )
    request = youtube.search().list(
        part="snippet",
        channelId=channel_id,
        maxResults=50,
        pageToken=next_page_token,
        type="video"
    )
    response = request.execute()

    video_ids = [item['id']['videoId'] for item in response['items']]
    video_ids_str = ','.join(video_ids)

    detail_request = youtube.videos().list(
        part="snippet,statistics",
        id=video_ids_str
    )
    detail_response = detail_request.execute()
    return response, detail_response


def get_channel_videos(channel, next_page_token=""):
    response, detail_responce = send_search_video_request(channel.channel_id)
    next_page_token = response.get('nextPageToken')
    items = detail_responce['items']
    while next_page_token:
        response, detail_responce = send_search_video_request(
            channel_id=channel.channel_id, 
            next_page_token=next_page_token
        )
        items += detail_responce['items']
        next_page_token = response.get('nextPageToken')
    print(items[0])
    return items

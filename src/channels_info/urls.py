from django.urls import path, re_path

from .views import (
    channels_view, videos_view, tags_view, channel_detail, channel_videos,
    VideoByTgaView, VideoDetailView
)

urlpatterns = [
    path('channels/', channels_view),
    path('videos/', videos_view),
    path('tags/', tags_view),
    path('channel_detail/<str:channel_id>/', channel_detail),
    path('channel_videos/<str:channel_id>/', channel_videos),
    re_path(r'^videos_by_tag/(?P<tags>[\w,-]+)/$', VideoByTgaView.as_view()),
    path('videos/video/<str:video_id>/', VideoDetailView.as_view())
]

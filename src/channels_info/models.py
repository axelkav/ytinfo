from django.db import models
from django.utils.timezone import now

from .utils import (
    get_yt_id_from_yt_username, get_channel_videos
)


class Tag(models.Model):
    """ Youtube video tags """
    name = models.CharField('Tag name', max_length=100, unique=True)

    class Meta:
        verbose_name = 'tag'
        verbose_name_plural = 'Tags'

    def __str__(self):
        return self.name


class Channel(models.Model):
    """ Youtube channels added from admin panel """
    url = models.URLField(
        'Youtube channel URL', 
        max_length=255,
        help_text='Put here the Youtube URL if ID is missing',
        unique=True
    )
    channel_id = models.CharField('Youtube ID', max_length=50, blank=True)

    class Meta:
        verbose_name = 'channel'
        verbose_name_plural = 'Channels'

    def __str__(self):
        return self.url

    def save(self, *args, **kwargs): 
        self.channel_id = self.get_channel_id()
        super().save(*args, **kwargs)

    def get_channel_id(self):
        if not self.channel_id:
            split_url = self.url.split('/')
            if split_url[3] == 'user':
                self.channel_id = get_yt_id_from_yt_username(split_url[4])
            else:
                self.channel_id = split_url[4]
            self.save()
        return self.channel_id

    def get_videos(self):
        videos = get_channel_videos(self)
        for video in videos:
            try:
                obj = VideoInfo.objects.get(
                    channel=self,
                    video_id=video['id'],
                    published=video['snippet']['publishedAt'],
                    title=video['snippet']['title'],
                )
            except VideoInfo.DoesNotExist:
                obj = VideoInfo.objects.create(
                    channel=self,
                    video_id=video['id'],
                    published=video['snippet']['publishedAt'],
                    title=video['snippet']['title'],
                )
                if 'tags' in video['snippet']:
                    for tag in video['snippet']['tags']:
                        try:
                            tag_obj = Tag.objects.get(name=tag)
                        except Tag.DoesNotExist:
                            tag_obj = Tag.objects.create(name=tag)
                        obj.tags.add(tag_obj)
            obj.save()
            VideoStatisticsHistory.objects.create(
                video=obj,
                view_count=video['statistics']['viewCount'],
                like_count=video['statistics']['likeCount'],
                dislike_count=video['statistics']['dislikeCount'],
                favorite_count=video['statistics']['favoriteCount'],
                comment_count=video['statistics']['commentCount'],
            )


class VideoInfo(models.Model):
    """ Youtube video information """
    channel = models.ForeignKey(
        Channel,
        related_name='videos',
        on_delete=models.CASCADE
    )
    video_id = models.CharField('VideoID', max_length=25, unique=True)
    published = models.DateTimeField('Published', default=now())
    title = models.CharField('Title', max_length=255)
    tags = models.ManyToManyField(Tag,
                                  related_name='videos',
                                  related_query_name='video')

    class Meta:
        verbose_name = 'video'
        verbose_name_plural = 'Videos'

    def __str__(self):
        return self.title

    @property
    def view_count(self):
        if self.stats.count() > 0:
            return self.stats.first().view_count
        return 0


class VideoStatisticsHistory(models.Model):
    """ Youtube videos statistics changes history """
    video = models.ForeignKey(
        VideoInfo, 
        related_name='stats',
        on_delete=models.CASCADE,
    )
    view_count = models.PositiveIntegerField('Views count', default=0)
    like_count = models.PositiveIntegerField('Likes count', default=0)
    dislike_count = models.PositiveIntegerField('Dislikes count', default=0)
    favorite_count = models.PositiveIntegerField('Favourite count', default=0)
    comment_count = models.PositiveIntegerField('Comments count', default=0)
    date_time = models.DateTimeField('DateTime', auto_now_add=True)

    class Meta:
        verbose_name = 'statistic'
        verbose_name_plural = 'Sattistics'
        ordering = ['date_time']

    def __str__(self):
        return f'Video "{self.video.title}" statistic'

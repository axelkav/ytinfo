from celery import shared_task

from channels_info.models import Channel


@shared_task
def check_channels_video_info():
    channels = Channel.objects.all()
    for channel in channels:
        channel.get_videos()

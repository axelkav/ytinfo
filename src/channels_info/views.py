from django.http import HttpResponse
from django.core.serializers import serialize
from django.shortcuts import get_object_or_404, get_list_or_404

from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView

from .models import Channel, VideoInfo, Tag, VideoStatisticsHistory

import logging

logger = logging.getLogger(__name__)


def channels_view(request):
    channels = serialize('json', Channel.objects.all())
    return HttpResponse(channels, content_type='application/json', status=200)


def videos_view(request):
    videos = serialize('json', VideoInfo.objects.all())
    return HttpResponse(videos, content_type='application/json', status=200)


def tags_view(request):
    tags = serialize('json', Tag.objects.all())
    return HttpResponse(tags, content_type='application/json', status=200)


def channel_detail(request, channel_id):
    channel = serialize(
        'json', [get_object_or_404(Channel, channel_id=channel_id),]
    )[1:-1]
    return HttpResponse(channel, content_type='application/json', status=200)


def channel_videos(request, channel_id):
    videos = serialize(
        'json', get_list_or_404(VideoInfo, channel__channel_id=channel_id)
    )
    return HttpResponse(videos, content_type='application/json', status=200)


class ChannelVideoView(ListAPIView):

    def get(self, request, channel_id, *args, **kwargs):
        channel = get_object_or_404(Channel, channel_id=channel_id)
        videos = get_list_or_404(VideoInfo, channel=channel_id)
        return Response(videos, status=status.HTTP_200_OK)
 

class VideoByTgaView(ListAPIView):

    def get(self, request, *args, **kwargs):
        print(args, kwargs)
        videos = get_list_or_404(VideoInfo, tags__name__in=args)
        return Response(videos, status=status.HTTP_200_OK)


class VideoDetailView(APIView):
    """Video detail view"""

    def get(self, request, video_id, *args, **kwargs):
        """Get user wallet balance or 404 if does not exist."""
        video_obj = get_object_or_404(VideoInfo, video_id=video_id)
        stats = VideoStatisticsHistory.objects.filter(video=video_obj).first()
        data = {
            'id': video_obj.id,
            'channel_url': video_obj.channel.url,
            'video_id': video_obj.video_id,
            'published': video_obj.published.strftime('%m/%d/%Y-%H:%M:%S'),
            'title': video_obj.title,
            'tags': [x.name for x in Tag.objects.filter(video=video_obj)]
        }
        if stats:        
            data['statistics'] = {
                'view_count': stats.view_count,
                'like_count': stats.like_count,
                'dislike_count': stats.dislike_count,
                'favorite_count': stats.favorite_count,
                'comment_count': stats.comment_count,
                'date_time': stats.date_time,
            }
        return Response(data, status=status.HTTP_200_OK)

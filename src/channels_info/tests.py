from django.test import TestCase
from django.conf import settings

from unittest.mock import patch, Mock

from .models import Channel
from .utils import (
    get_yt_id_from_yt_username, send_search_video_request
)

from googleapiclient.discovery import build

import json
import os

with open('./src/channels_info/responce.json') as f:
    YOUTUBE_CHANNEL_FROM_USERNAME = json.loads(f.read())


class UtilsTestCase(TestCase):
    """ tests for utils """

    @patch('googleapiclient.discovery.Resource')
    def test_get_yt_id_from_yt_username(self, MockResource, *args, **kwargs):
        """
        get channel id with username
        """
        username = 'GoogleDevelopers'
        youtube = build("youtube", "v3", developerKey=settings.GOOGLE_YT_API_KEY)
        with patch(youtube.channels.list.execute()) as y_res:
            y_res.return_value = YOUTUBE_CHANNEL_FROM_USERNAME
            res = get_yt_id_from_yt_username(username)
            self.assertEqual(res[0]['id'], 'UC_x5XG1OV2P6uZZ5FSM9Ttw')


class ChannelTestCase(TestCase):
    """Tests for Channel model """
    def setUp(self):
        pass

    @patch('googleapiclient.discovery.build_from_document',
           return_value=YOUTUBE_CHANNEL_FROM_USERNAME)
    def test_get_channel_id_from_username(self, *args, **kwargs):
        """
        getting channel id from api with username for old youtube channels 
        """
        self.channel = Channel.objects.create(
            url='https://www.youtube.com/user/GoogleDevelopers'
        )
        self.assertEqual(self.channel.chaneel_id, 'UC_x5XG1OV2P6uZZ5FSM9Ttw')

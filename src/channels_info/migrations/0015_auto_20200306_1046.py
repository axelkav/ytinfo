# Generated by Django 3.0.3 on 2020-03-06 10:46

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('channels_info', '0014_auto_20200306_1008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='videoinfo',
            name='published',
            field=models.DateTimeField(default=datetime.datetime(2020, 3, 6, 10, 46, 29, 635663, tzinfo=utc), verbose_name='Published'),
        ),
        migrations.AlterField(
            model_name='videoinfo',
            name='tags',
            field=models.ManyToManyField(related_name='tags', related_query_name='tag', to='channels_info.Tag'),
        ),
    ]

# Generated by Django 2.2 on 2020-03-03 08:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(help_text='Put here the Youtube URL if ID is missing', max_length=255, unique=True, verbose_name='Youtube channel URL')),
                ('channel_id', models.CharField(blank=True, max_length=50, verbose_name='Youtube ID')),
            ],
        ),
        migrations.CreateModel(
            name='VideoInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('channel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='videos', to='channels_info.Channel')),
            ],
        ),
    ]

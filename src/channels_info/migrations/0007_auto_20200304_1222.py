# Generated by Django 2.2 on 2020-03-04 12:22

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('channels_info', '0006_auto_20200304_1151'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tag',
            name='videos',
        ),
        migrations.AddField(
            model_name='videoinfo',
            name='tags',
            field=models.ManyToManyField(related_name='videos', to='channels_info.Tag'),
        ),
        migrations.AlterField(
            model_name='videoinfo',
            name='published',
            field=models.DateTimeField(default=datetime.datetime(2020, 3, 4, 12, 22, 40, 847561), verbose_name='Published'),
        ),
    ]

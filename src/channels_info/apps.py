from django.apps import AppConfig


class ChannelsInfoConfig(AppConfig):
    name = 'channels_info'

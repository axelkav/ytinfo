from django.contrib import admin

from channels_info.models import Channel, VideoInfo, Tag, VideoStatisticsHistory


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    readonly_fields = ['channel_id', ]


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(VideoInfo)
class VideoInfoAdmin(admin.ModelAdmin):
    list_display = ('title', 'view_count')
    search_fields = ['title', ]
    filter_horizontal = ['tags', ]
    list_filter = ['channel', 'published']

@admin.register(VideoStatisticsHistory)
class VideoStatisticsHistory(admin.ModelAdmin):
    pass

# Django based application to collect information about youtube channels

## Every 10 minutes will be request to get videos and information about video for all channels added from admin page

### Requirements

This project require:

* git >= 2.10.0
* docker >= 17.12
* docker-compose >= 1.18.0

### To run project

!!!Those command examples for linux based OS

Clone project from repository:

```shell
git clone git@bitbucket.org:axelkav/ytinfo.git
```

Go to project folder:

```shell
cd ytinfo/
```

To build images and run project:

```shell
make up
```

To apply migrations:

```shell
make syncdb
```

To create superuser:

```shell
make superuser
```

In your browser go to: http://127.0.0.1:8000/admin

Login with superuser credettials created earlier

In the admin panel go to: http://127.0.0.1:8000/admin/channels_info/channel/

To add a channel click ***add channel*** button and input channel URl to ***Youtube channel URL*** field

### URL list

* api/videos
* api/tags
* api/channel_detail/<channel_id>
* api/channel_videos/<channel_id>
* api/videos/video/<video_id>
